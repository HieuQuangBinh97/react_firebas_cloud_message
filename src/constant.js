const subscribeTokenTopicURL = 'http://localhost:4000/firebase/token/subscribe';
const unSubscribeTokenFromTopicURL = 'http://localhost:400/firebase/token/unsubscribe';
const TokenUser = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySUQiOiIyZTBiNzZiZS1hNzM5LTRiMzctYTdhMS1jZmQ4Y2NkMmJiYzQiLCJmdWxsTmFtZSI6Im1hdGkiLCJzdGF0dXMiOiJBQ1RJVkUiLCJpYXQiOjE2MzQwMDUxODEsImV4cCI6MTY0MTc4MTE4MX0.7Wx3BsnRqhS-tcat-_RH-j9qni6VRhxV9rZN1mWiwq0';

export {
  subscribeTokenTopicURL,
  unSubscribeTokenFromTopicURL,
  TokenUser,
};