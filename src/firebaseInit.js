/* eslint-disable no-console */
import firebase from 'firebase/app';
import 'firebase/messaging';

const config = {
  apiKey: "AIzaSyDs5Jsi1JDzrlh1HS6TCr81cFxRmG_4SS8",
  authDomain: "prodeej-test.firebaseapp.com",
  databaseURL: 'https://prodeej-test-default-rtdb.firebaseio.com',
  projectId: "prodeej-test",
  storageBucket: "prodeej-test.appspot.com",
  messagingSenderId: "685044997744",
  appId: "1:685044997744:web:1ed7c8615698d068909eb5",
  measurementId: "G-KNCKBZ308Q"
};

firebase.initializeApp(config);

export const messaging = firebase.messaging();

messaging.usePublicVapidKey('BAjTjH3F7vikUxLCgPYxkGD3KwTKh4cu9etPwyM4P6Qhk9is4KzLAqZKjYm5aUbm_mtLmEfzi6JYYkjiIwRlhk0',);

export const requestFirebaseNotificationGetToken = async () => {
  try {
    const getToken = await messaging.getToken();
  
    return getToken;
  } catch (error) {
    console.log(error);
  }
};

export const requestFirebaseNotificationDeleteToken = async () => {
  try {
    const getToken = await messaging.deleteToken();
  
    return getToken;
  } catch (error) {
    console.log(error);
  }
};
